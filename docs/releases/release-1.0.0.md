# Relseas Notes
## Version 1.0.0

#### Table of Contents
1. [Configuration](#configuration)
2. [APIs](#apis)

## Configuration

To start TestBed API and its DB please fill `.env` file with required variables and execute `docker-compose.yml`

These variables are necessary, because the private keys and mnemonics in the solution of Spherity are encrypted using AWS KMS.
Required variables:
- AWS_KEY_ID (AWS KMS key ID*) 
- AWS_REGION
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY

**AWS KMS key ID***
To get the AWS_KEY_ID follow these steps in AWS:
AWS Key Management Service => Create  key => Symmetric => Next => next => next => finish
during creating key you can configure all additional options, permissions etc. Then review policy which will be created automatically and create key.
After this key will appear in table and you can take key id
![](images/AWS_KEY_ID.png)

**Execute**

```
docker-compose -f docker-compose.yml up
```

## APIs
- Generate Authorization token
- Generate DID Auth (x-did-auth) token
- Identity Vaults API - Create Vault
- Identity Vaults API - Derive new wallet
- Identity Vaults API - Get Vaults
- Identity Vaults API - Delete Vault
- Wallet API - Get Wallet
- Wallet API - Rename Wallet
- Roles API - Create Role
- Roles API - Get Role
- Roles API - Update Roles
- Roles API - Delete Roles
- Role Assignments API - Create Assignments
- Role Assignments API - Get Assignments
- VCs API - Sign VCs
- VCs API - Store VCs
- VCs API - Get VCs
- VCs API - Verify VCs
- VCs API - Delete VCs